local circle = display.newCircle(display.contentCenterX, display.contentCenterY, 100)
circle.fill = {0, 0, 1, 0.5}

local function touchEventListener(event)
	if event.phase == "began" then
    changecolor = timer.performWithDelay( 100,
      function()
        circle.fill = {math.random(255)/255, math.random(255)/255, math.random(255)/255}
      end
    ,0)
	else if event.phase == "ended" then
		timer.cancel( changecolor )
	end
  end
end

circle:addEventListener("touch", touchEventListener)
